
# Movie-SQL

This project was about learning Spring Data JPA, Spring Web, Swagger, Lombok, Docker. Using these we will be running Swagger to manipulate Tables From a running Postgres Server that has been seeded by data.sql file.
## Installation

Movies-sql with Git clone with local IDE



```bash
    git clone git@gitlab.com:Abose/movies-sql.git
    Import the project into your Java IDE
    and Run main.java
    go to http://localhost:8080/swagger-ui/index.html#/
```

## Prerequisites Local IDE
* Java 17
* IntelliJ IDEA Ultimate
* Postgres Server running with empty DATABASE movies with port 5432
* application.properties change username, password to match Server


Run movies-sql with docker

```
    git clone git@gitlab.com:Abose/movies-sql.git
    cd to root of folder
    Run in Command or Terminal: Docker compose up
    go to http://localhost:8080/swagger-ui/index.html#/
```

## Prerequisites Run With Docker
* Docker

## Features

- Java
- Spring Data JPA
- Spring Web
- Swagger
- PostgreSQL
- Lombok
- Docker


## API Reference

#### Get all *items

```http
  GET /api/v1/*items
```

*  (*) can be movies, Characters, franchises

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `None` |          | **None**. Dont need to add anything to get all |



#### Get item

```http
  GET /api/v1/items/${id}
```

*  (*) can be movies, Characters, franchises

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `Int or String` | **Required**. Id of item to fetch/get |


#### Put item

```http
  PUT /api/v1/*items/${id}
```

*  (*) can be movies, Characters, franchises

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `Int or String` | **Required**. Id of item to put |

Put Body depends on which you want to put but it will be easy to change inside Swagger, it shows what you can put

#### Post item

```http
  POST /api/v1/*items/${id}
```

*  (*) can be movies, Characters, franchises

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `Int or String` | **Required**. Id of item to Post |

Post Body depends on which you want to Post but it will be easy to change inside Swagger, it Example and you can edit it to Post 


#### Delete item

```http
  DELETE /api/v1/*items/${id}
```

*  (*) can be movies, Characters, franchises

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `Int or String` | **Required**. Id of item to Delete |


## 🚀 About Me
i am going on a Noroff Course to become full stack developer...


## License

[MIT](https://choosealicense.com/licenses/mit/)

