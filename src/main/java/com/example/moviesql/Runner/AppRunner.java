package com.example.moviesql.Runner;


import com.example.moviesql.Repository.FranchiseRepo;
import com.example.moviesql.Repository.MovieRepo;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class AppRunner implements ApplicationRunner {
    private final MovieRepo movieRepo;

    private final FranchiseRepo franchiseRepo;

    public AppRunner(MovieRepo movieRepo, FranchiseRepo franchiseRepo) {
        this.movieRepo = movieRepo;
        this.franchiseRepo = franchiseRepo;
    }

    // Added AppRunner for future usage and testing.
    @Override
    public void run(ApplicationArguments args) {
        System.out.println("AppRunner is running");
    }

}
