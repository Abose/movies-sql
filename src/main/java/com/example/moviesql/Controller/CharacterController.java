package com.example.moviesql.Controller;


import com.example.moviesql.Mappers.CharacterMapper;
import com.example.moviesql.Models.Character;
import com.example.moviesql.Models.Dtos.Character.CharacterDto;
import com.example.moviesql.Models.Dtos.Character.CharacterDtoAdd;
import com.example.moviesql.Models.Dtos.Character.CharacterDtoGet;
import com.example.moviesql.Service.Interface.CharacterService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;


// standard path to use character controller.
@RestController
@Tag(name = "Character")
@RequestMapping(path = "api/v1/characters")
public class CharacterController {
    // needed imports and renamed.
    private final CharacterService characterService;
    private final CharacterMapper characterMapper;

    // auto set up CharacterService and CharacterMapper
    public CharacterController(CharacterService characterService, CharacterMapper characterMapper) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
    }

    // method to search by id character and get back result.
    // added ApiResponse to get result.
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDto.class)
                    )
                    }
            ),
            @ApiResponse(responseCode = "404",
                    description = "character does not exist with given ID",
                    content = @Content
            )
    })
    @GetMapping("{id}")
    @Operation(description = "id of character", summary = "search by id")
    public ResponseEntity<CharacterDtoGet> getById(
            @PathVariable int id
    ) {
        Character chara = characterService.findById(id);
        CharacterDtoGet characterDto = characterMapper.characterToCharacterDtoGet(chara);
        return ResponseEntity.ok().body(characterDto);
    }

    // method to delete Character by id
    // added ApiResponse to get result.
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDto.class)
                    )
                    }
            ),
            @ApiResponse(responseCode = "404",
                    description = "character does not exist with given ID",
                    content = @Content
            )
    })
    @DeleteMapping("{id}")
    @Operation(description = "delete character in database", summary = "delete by id")
    public ResponseEntity deleteById(
            @PathVariable int id
    ) {
        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    // method to get all Character in table.
    // added ApiResponse to get result.
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDto.class)
                    )
                    }
            ),
            @ApiResponse(responseCode = "404",
                    description = "character table does not exist",
                    content = @Content
            )
    })
    @GetMapping()
    @Operation(description = "shows all characters in database", summary = "show all characters")
    public ResponseEntity<Set<CharacterDtoGet>> findAll() {
        return ResponseEntity.ok().body(
                characterService.findAll().stream()
                        .map(character -> characterMapper.characterToCharacterDtoGet(character))
                        .collect(Collectors.toSet())
        );
    }

    // method to update Character by id
    // added ApiResponse to get result.
    @Operation(summary = "update data for a character", description = "update character in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "character successfully updated",
                    content = @Content
            ),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content
            ),
            @ApiResponse(responseCode = "404",
                    description = "character not found with given ID",
                    content = @Content
            )
    })
    @PutMapping("{id}")
    public ResponseEntity updateCharacter(
            @RequestBody CharacterDto characterDto,
            @PathVariable int id
    ) {
        if (id != characterDto.getId()) {
            return ResponseEntity.badRequest().build();
        }
        Character chara = characterMapper.characterDtoToCharacter(characterDto);
        characterService.update(chara);
        return ResponseEntity.noContent().build();
    }

    // method to new add character
    // added ApiResponse to get result.
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Add successfully",
                    content = @Content
            ),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content
            )
    })
    @PostMapping
    @Operation(summary = "add new character", description = "add character in database")
    public ResponseEntity<URI> addCharacter(@RequestBody CharacterDtoAdd characterDtoAdd) {
        Character chara = characterMapper.characterDtoAddToCharacter(characterDtoAdd);
        characterService.add(chara);
        URI location = URI.create("api/v1/characters" + chara.getId());
        return ResponseEntity.created(location).build();
    }

}
