package com.example.moviesql.Controller;

import com.example.moviesql.Mappers.CharacterMapper;
import com.example.moviesql.Mappers.MovieMapper;
import com.example.moviesql.Models.Dtos.Character.CharacterDto;
import com.example.moviesql.Models.Dtos.Movie.MovieDto;
import com.example.moviesql.Models.Dtos.Movie.MovieDtoAdd;
import com.example.moviesql.Models.Dtos.Movie.MovieDtoGet;
import com.example.moviesql.Models.Movie;
import com.example.moviesql.Service.Interface.MovieService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;


@RestController
@Tag(name = "Movie")
@RequestMapping(path = "api/v1/movies")

public class MovieController {

    // needed imports and renamed.
    private final MovieService movieService;
    private final MovieMapper movieMapper;
    private final CharacterMapper characterMapper;

    // auto set up CharacterService and CharacterMapper
    public MovieController(MovieService movieService, MovieMapper movieMapper, CharacterMapper characterMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
        this.characterMapper = characterMapper;
    }

    // method to update character inside movie
    // added ApiResponse to get result.
    @Operation(summary = "edit character by id of movie. ", description = "Update the list of characters in movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Update successfully",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = MovieDto.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = MovieDto.class)
                    )),
    })
    @PutMapping("{id}/characters")
    public ResponseEntity updateCharacters(@PathVariable int id, @RequestBody int[] charactersId) {
        movieService.updateCharacters(id, charactersId);
        return ResponseEntity.noContent().build();
    }

    // method to search by id and get result
    // added ApiResponse to get result.
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDto.class)
                    )
                    }
            ),
            @ApiResponse(responseCode = "404",
                    description = "movie does not exist with given ID",
                    content = @Content
            )
    })
    @GetMapping("{id}")
    @Operation(description = "id of movie", summary = "search by id")
    public ResponseEntity<MovieDtoGet> getById(
            @PathVariable int id
    ) {
        Movie mov = movieService.findById(id);
        MovieDtoGet movieDtoGet = movieMapper.movieToMovieDtoGet(mov);
        return ResponseEntity.ok().body(movieDtoGet);
    }

    // method to delete by id
    // added ApiResponse to get result.
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDto.class)
                    )
                    }
            ),
            @ApiResponse(responseCode = "404",
                    description = "movie does not exist with given ID",
                    content = @Content
            )
    })
    @DeleteMapping("{id}")
    @Operation(description = "delete movie by id of movie", summary = "delete by id")
    public ResponseEntity deleteById(
            @PathVariable int id
    ) {
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    // method to get/show all movies
    // added ApiResponse to get result.
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDto.class)
                    )
                    }
            ),
            @ApiResponse(responseCode = "404",
                    description = "movie table does not exist",
                    content = @Content
            )
    })
    @GetMapping()
    @Operation(description = "shows all movies in database", summary = "show all movies")
    public ResponseEntity<Set<MovieDtoGet>> findAll() {
        return ResponseEntity.ok().body(
                movieService.findAll().stream()
                        .map(movie -> movieMapper.movieToMovieDtoGet(movie))
                        .collect(Collectors.toSet())
        );
    }

    // method to update movie
    // added ApiResponse to get result.
    @Operation(summary = "update data for a movie", description = "update movie in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "movie successfully updated",
                    content = @Content
            ),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content
            ),
            @ApiResponse(responseCode = "404",
                    description = "movie not found with given ID",
                    content = @Content
            )
    })
    @PutMapping("{id}")
    public ResponseEntity updateMovie(
            @RequestBody MovieDto movieDto,
            @PathVariable int id
    ) {
        if (id != movieDto.getId()) {
            return ResponseEntity.badRequest().build();
        }
        Movie mov = movieMapper.movieDtoToMovie(movieDto);
        movieService.update(mov);
        return ResponseEntity.noContent().build();
    }

    // method to add new movie.
    // added ApiResponse to get result.
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Add successfully",
                    content = @Content
            ),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content
            )
    })
    @PostMapping
    @Operation(summary = "add new movie", description = "add movie in database")
    public ResponseEntity<URI> addMovie(@RequestBody MovieDtoAdd movieDtoAdd) {
        Movie mov = movieMapper.movieToMovieDtoAdd(movieDtoAdd);
        movieService.add(mov);
        URI location = URI.create("api/v1/movies" + mov.getId());
        return ResponseEntity.created(location).build();
    }


    @Operation(summary = "get a list of characters per movie", description = "Get a list of all characters that appear in a given movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = CharacterDto.class)))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json"
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json"
                    )),
    })
    @GetMapping("{id}/characters")
    public ResponseEntity getAllCharacters(@PathVariable int id) {
        return ResponseEntity.ok(
                characterMapper.movieStaringCharacters(
                        movieService.findAllCharacters(id)
                ));
    }


}
