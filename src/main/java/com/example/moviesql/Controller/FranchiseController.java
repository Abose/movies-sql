package com.example.moviesql.Controller;


import com.example.moviesql.Mappers.CharacterMapper;
import com.example.moviesql.Mappers.FranchiseMapper;
import com.example.moviesql.Mappers.MovieMapper;
import com.example.moviesql.Models.Dtos.Character.CharacterDto;
import com.example.moviesql.Models.Dtos.Franchise.FranchiseDto;
import com.example.moviesql.Models.Dtos.Franchise.FranchiseDtoAdd;
import com.example.moviesql.Models.Dtos.Franchise.FranchiseDtoGet;
import com.example.moviesql.Models.Dtos.Movie.MovieDtoAdd;
import com.example.moviesql.Models.Franchise;
import com.example.moviesql.Service.Interface.FranchiseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@Tag(name = "Franchise")
@RequestMapping(path = "api/v1/franchises")
public class FranchiseController {

    // needed imports and renamed.
    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;

    private final CharacterMapper characterMapper;
    private final MovieMapper movieMapper;

    // auto set up CharacterService and CharacterMapper
    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper, CharacterMapper characterMapper, MovieMapper movieMapper) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
        this.characterMapper = characterMapper;
        this.movieMapper = movieMapper;
    }

    // method to get/search by id
    // added ApiResponse to get result.
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDto.class)
                    )
                    }
            ),
            @ApiResponse(responseCode = "404",
                    description = "franchise does not exist with given ID",
                    content = @Content
            )
    })
    @GetMapping("{id}")
    @Operation(description = "id of franchise", summary = "search by id")
    public ResponseEntity<FranchiseDtoGet> getById(
            @PathVariable int id
    ) {
        Franchise franchise = franchiseService.findById(id);
        FranchiseDtoGet franchiseDto = franchiseMapper.franchiseTofranchiseDtoGet(franchise);
        return ResponseEntity.ok().body(franchiseDto);
    }

    // method to delete franchise by id
    // added ApiResponse to get result.
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDto.class)
                    )
                    }
            ),
            @ApiResponse(responseCode = "404",
                    description = "franchise does not exist with given ID",
                    content = @Content
            )
    })
    @DeleteMapping("{id}")
    @Operation(description = "delete franchise in database", summary = "delete by id")
    public ResponseEntity deleteById(
            @PathVariable int id
    ) {
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    // method to show/get all Franchise
    // added ApiResponse to get result.
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDto.class)
                    )
                    }
            ),
            @ApiResponse(responseCode = "404",
                    description = "franchise table does not exist",
                    content = @Content
            )
    })
    @GetMapping()
    @Operation(description = "shows all franchise in database", summary = "show all franchises")
    public ResponseEntity<Set<FranchiseDtoGet>> findAll() {
        return ResponseEntity.ok().body(
                franchiseService.findAll().stream()
                        .map(franchise -> franchiseMapper.franchiseTofranchiseDtoGet(franchise))
                        .collect(Collectors.toSet())
        );
    }

    // method to update franchise
    // added ApiResponse to get result.
    @Operation(summary = "update data for a franchise", description = "update franchise in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "franchise successfully updated",
                    content = @Content
            ),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content
            ),
            @ApiResponse(responseCode = "404",
                    description = "franchise not found with given ID",
                    content = @Content
            )
    })
    @PutMapping("{id}")
    public ResponseEntity updateFranchise(
            @RequestBody FranchiseDto franchiseDto,
            @PathVariable int id
    ) {
        if (id != franchiseDto.getId()) {
            return ResponseEntity.badRequest().build();
        }
        Franchise fran = franchiseMapper.franchiseDtoToFranchise(franchiseDto);
        franchiseService.update(fran);
        return ResponseEntity.noContent().build();
    }

    // method to add new connection to movie from franchise
    // added ApiResponse to get result.
    @Operation(summary = "update existing franchise", description = "update existing franchise to add more franchise to movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Update Successfully",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDto.class)
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDto.class)
                    )),
    })
    @PutMapping("{id}/movies")
    public ResponseEntity updateMovies(@PathVariable int id, @RequestBody int[] movieId) {
        franchiseService.updateMovies(id, movieId);
        return ResponseEntity.noContent().build();
    }

    // method to add new franchise
    // added ApiResponse to get result.
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Add successfully",
                    content = @Content
            ),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content
            )
    })
    @PostMapping
    @Operation(summary = "add new franchise", description = "add franchise in database")
    public ResponseEntity<URI> addFranchise(@RequestBody FranchiseDtoAdd franchiseDtoAdd) {
        Franchise fran = franchiseMapper.franchiseDtoAddToFranchise(franchiseDtoAdd);
        franchiseService.add(fran);
        URI location = URI.create("api/v1/franchises" + fran.getId());
        return ResponseEntity.created(location).build();
    }

    @Operation(description = "get a list of all movies owned by a given franchise", summary = "get list of movies inside franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = MovieDtoAdd.class)))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json"
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json"
                    )),
    })
    @GetMapping("{id}/movies")
    public ResponseEntity getAllMovies(@PathVariable int id) {
        return ResponseEntity.ok(
                movieMapper.movieToFranchiseDtoGet(franchiseService.findAllMovies(id)));
    }

    @Operation(description = "get a list of all characters owned by a given franchise", summary = "get list of Characters inside franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = CharacterDto.class)))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content(
                            mediaType = "application/json"
                    )),
            @ApiResponse(responseCode = "404",
                    description = "Not found",
                    content = @Content(
                            mediaType = "application/json"
                    )),
    })
    @GetMapping("{id}/characters")
    public ResponseEntity getAllMoviesFromFranchise(@PathVariable int id) {
        return ResponseEntity.ok(
                franchiseMapper.getAllCharactersFromFranchise(franchiseService.findAllCharacters(id)));
    }


}
