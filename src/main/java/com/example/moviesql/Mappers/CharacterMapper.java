package com.example.moviesql.Mappers;


import com.example.moviesql.Models.Character;
import com.example.moviesql.Models.Dtos.Character.CharacterDto;
import com.example.moviesql.Models.Dtos.Character.CharacterDtoAdd;
import com.example.moviesql.Models.Dtos.Character.CharacterDtoGet;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper(componentModel = "spring")
public interface CharacterMapper {

    // used to map CharacterDto to character
    CharacterDto characterToCharacterDto(Character character);

    // used to map Character to characterDto
    Character characterDtoToCharacter(CharacterDto characterDto);

    // used to map CharacterDtoGet to character made 1 more to make it not loop.
    CharacterDtoGet characterToCharacterDtoGet(Character character);

    // used add to show characters inside movie
    Character characterDtoAddToCharacter(CharacterDtoAdd characterDtoAdd);

    // used add to show characters inside movie
    Set<CharacterDto> movieStaringCharacters(Set<Character> character);
}
