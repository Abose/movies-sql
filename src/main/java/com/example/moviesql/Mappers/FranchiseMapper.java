package com.example.moviesql.Mappers;


import com.example.moviesql.Models.Character;
import com.example.moviesql.Models.Dtos.Franchise.FranchiseDto;
import com.example.moviesql.Models.Dtos.Franchise.FranchiseDtoAdd;
import com.example.moviesql.Models.Dtos.Franchise.FranchiseDtoGet;
import com.example.moviesql.Models.Dtos.Franchise.FranchiseDtoId;
import com.example.moviesql.Models.Franchise;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper(componentModel = "spring")
public interface FranchiseMapper {
    // used to map FranchiseDto to Franchise
    FranchiseDto franchiseTofranchiseDto(Franchise franchise);

    // used to map Franchise to FranchiseDto
    Franchise franchiseDtoToFranchise(FranchiseDto franchiseDto);

    // used to map FranchiseDtoGet to Franchise, to show simplified version without it looping
    FranchiseDtoGet franchiseTofranchiseDtoGet(Franchise franchise);

    // used to make connection from franchise to FranchiseDtoAdd
    Franchise franchiseDtoAddToFranchise(FranchiseDtoAdd franchiseDtoAdd);

    // used to get all characters from franchise.
    Set<FranchiseDtoId> getAllCharactersFromFranchise(Set<Character> franchiseDto);

}
