package com.example.moviesql.Mappers;


import com.example.moviesql.Models.Dtos.Movie.MovieDto;
import com.example.moviesql.Models.Dtos.Movie.MovieDtoAdd;
import com.example.moviesql.Models.Dtos.Movie.MovieDtoGet;
import com.example.moviesql.Models.Movie;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper(componentModel = "spring")
public interface MovieMapper {
    // used to map MovieDto to Movie
    MovieDto movieToMovieDto(Movie movie);

    // used to map Movie to MovieDto
    Movie movieDtoToMovie(MovieDto movieDto);

    // used to map MovieDtoGet to Movie, to show simplified version without it looping
    MovieDtoGet movieToMovieDtoGet(Movie movie);

    Movie movieToMovieDtoAdd(MovieDtoAdd MovieDtoAdd);

    Set<MovieDtoAdd> movieToFranchiseDtoGet(Set<Movie> movie);
}
