package com.example.moviesql.Models;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Entity
@Getter
@Setter
@ToString
public class Movie {
    // made java Hibernate auto make tablet with these data.
    @ManyToMany
    @JoinTable(name = "movie_characters", joinColumns = {@JoinColumn(name = "movie_id")}, inverseJoinColumns = {@JoinColumn(name = "character_id")})
    Set<Character> characters;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private int id;
    @Column(name = "movie_title", length = 60, nullable = false)
    private String movieTitle;
    @Column(name = "genre", length = 60, nullable = false)
    private String genre;
    @Column(name = "release_year", length = 20, nullable = false)
    private int releaseYear;
    @Column(name = "director", length = 50, nullable = true)
    private String director;
    @Column(name = "picture", nullable = true)
    private String picture;
    @Column(name = "trailer", nullable = true)
    private String trailer;
    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;

}
