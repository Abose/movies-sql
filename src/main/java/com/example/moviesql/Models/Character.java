package com.example.moviesql.Models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;


@Entity
@Getter
@Setter
@ToString
public class Character {

    // made java Hibernate auto make tablet with these data.
    @ManyToMany(mappedBy = "characters")
    Set<Movie> movies;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "character_id")
    private int id;
    @Column(name = "full_name", length = 40, nullable = false)
    private String name;
    @Column(name = "alias", length = 40, nullable = true)
    private String alias;
    @Column(name = "gender", length = 30, nullable = false)
    private String gender;
    @Column(name = "picture", nullable = true)
    private String picture;
}
