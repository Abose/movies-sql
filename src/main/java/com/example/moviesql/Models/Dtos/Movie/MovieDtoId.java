package com.example.moviesql.Models.Dtos.Movie;


import lombok.Data;

@Data
public class MovieDtoId {
    // simple Dto class to get only data that is needed
    private int id;
    private String movieTitle;
}
