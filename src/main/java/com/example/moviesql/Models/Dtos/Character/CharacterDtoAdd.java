package com.example.moviesql.Models.Dtos.Character;

import lombok.Data;

@Data
public class CharacterDtoAdd {
    private String name;
    private String alias;
    private String gender;
}
