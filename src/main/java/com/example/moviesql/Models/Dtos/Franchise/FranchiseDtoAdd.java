package com.example.moviesql.Models.Dtos.Franchise;

import lombok.Data;

@Data
public class FranchiseDtoAdd {
    private String name;

    private String description;
}
