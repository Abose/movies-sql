package com.example.moviesql.Models.Dtos.Character;


import lombok.Data;

@Data
public class CharacterDto {
    // simple Dto class to get only data that is needed
    private int id;

    private String name;

    private String alias;

    private String gender;

}
