package com.example.moviesql.Models.Dtos.Movie;

import com.example.moviesql.Models.Dtos.Franchise.FranchiseDtoId;
import lombok.Data;

@Data
public class MovieDtoGet {
    // simple Dto class to get only data that is needed
    private int id;
    private String movieTitle;
    private String genre;
    private int releaseYear;
    private String director;
    private FranchiseDtoId franchise;
}
