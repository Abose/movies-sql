package com.example.moviesql.Models.Dtos.Franchise;

import com.example.moviesql.Models.Dtos.Movie.MovieDtoId;
import lombok.Data;

import java.util.Set;

@Data
public class FranchiseDtoGet {
    Set<MovieDtoId> movies;
    // simple Dto class to get only data that is needed
    private int id;
    private String name;
    private String description;
}
