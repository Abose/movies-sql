package com.example.moviesql.Models.Dtos.Franchise;


import lombok.Data;

@Data
public class FranchiseDto {
    // simple Dto class to get only data that is needed
    private int id;

    private String name;

    private String description;

}
