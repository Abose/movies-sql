package com.example.moviesql.Models.Dtos.Movie;


import lombok.Data;

@Data
public class MovieDtoAdd {
    // simple Dto class to get only data that is needed
    private String movieTitle;
    private String genre;

    private int releaseYear;

    private String director;
}
