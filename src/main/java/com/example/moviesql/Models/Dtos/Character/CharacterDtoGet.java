package com.example.moviesql.Models.Dtos.Character;


import com.example.moviesql.Models.Dtos.Movie.MovieDtoId;
import lombok.Data;

import java.util.Set;

@Data
public class CharacterDtoGet {
    Set<MovieDtoId> movies;
    // simple Dto class to get only data that is needed
    private int id;
    private String name;
    private String alias;
    private String gender;

}
