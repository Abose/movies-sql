package com.example.moviesql.Models.Dtos.Franchise;


import lombok.Data;

@Data
public class FranchiseDtoId {

    // choose to add name as well for make it more readable.
    private int id;

    private String name;

}
