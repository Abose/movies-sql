package com.example.moviesql.Models.Dtos.Movie;


import lombok.Data;

@Data
public class MovieDto {
    // simple Dto class to get only data that is needed
    private int id;
    private String movieTitle;
    private String genre;

    private int releaseYear;

    private String director;
}
