package com.example.moviesql.Models;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Entity
@Getter
@Setter
@ToString
public class Franchise {
    // made java Hibernate auto make tablet with these data.
    @OneToMany(mappedBy = "franchise")
    Set<Movie> movies;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "franchise_id")
    private int id;
    @Column(name = "name", length = 40, nullable = false)
    private String name;
    @Column(name = "description", columnDefinition = "Text", nullable = false)
    private String description;
}
