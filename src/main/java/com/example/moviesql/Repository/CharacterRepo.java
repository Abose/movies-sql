package com.example.moviesql.Repository;

import com.example.moviesql.Models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// Repository take make use of JpaRepository
@Repository
public interface CharacterRepo extends JpaRepository<Character, Integer> {
}
