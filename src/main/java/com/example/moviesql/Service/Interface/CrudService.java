package com.example.moviesql.Service.Interface;

import java.util.Collection;

// made full Crud that is extended and used in all Service
public interface CrudService<T, ID> {

    T findById(ID id);

    Collection<T> findAll();

    T add(T entity);

    T update(T entity);

    void delete(T entity);

    void deleteById(ID id);

}
