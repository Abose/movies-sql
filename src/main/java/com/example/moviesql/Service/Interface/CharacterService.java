package com.example.moviesql.Service.Interface;


import com.example.moviesql.Models.Character;

// Repository take make use of JpaRepository to extend Crud
public interface CharacterService extends CrudService<Character, Integer> {
}
