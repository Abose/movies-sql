package com.example.moviesql.Service.Interface;

import com.example.moviesql.Models.Character;
import com.example.moviesql.Models.Franchise;
import com.example.moviesql.Models.Movie;

import java.util.Set;

// Repository take make use of JpaRepository to extend Crud
public interface FranchiseService extends CrudService<Franchise, Integer> {

    // added extra method to be able to link them
    void updateMovies(int franchiseId, int[] movieId);

    // added extra method to be able to link them
    Set<Movie> findAllMovies(int franchiseId);

    // added extra method to be able to link them
    Set<Character> findAllCharacters(int franchiseId);
}
