package com.example.moviesql.Service.Interface;

import com.example.moviesql.Models.Character;
import com.example.moviesql.Models.Movie;

import java.util.Set;

// Repository take make use of JpaRepository to extend Crud
public interface MovieService extends CrudService<Movie, Integer> {
    // added extra method to be able to link them
    void updateCharacters(int movieId, int[] characterId);

    Set<Character> findAllCharacters(int movieId);
}
