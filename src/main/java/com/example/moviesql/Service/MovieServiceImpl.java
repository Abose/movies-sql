package com.example.moviesql.Service;

import com.example.moviesql.Models.Character;
import com.example.moviesql.Models.Movie;
import com.example.moviesql.Repository.CharacterRepo;
import com.example.moviesql.Repository.FranchiseRepo;
import com.example.moviesql.Repository.MovieRepo;
import com.example.moviesql.Service.Interface.MovieService;
import com.example.moviesql.Utils.Exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


@Service
public class MovieServiceImpl implements MovieService {
    // Override them to add Method to each ServiceImpl
    private final MovieRepo movieRepo;
    private final FranchiseRepo franchiseRepo;

    private final CharacterRepo characterRepo;

    public MovieServiceImpl(MovieRepo movieRepo,
                            FranchiseRepo franchiseRepo, CharacterRepo characterRepo) {
        this.movieRepo = movieRepo;
        this.franchiseRepo = franchiseRepo;
        this.characterRepo = characterRepo;
    }

    // method to find by id that uses by Crud
    @Override
    public Movie findById(Integer id) {
        if (movieRepo.existsById(id)) {
            return movieRepo.findById(id).get();
        } else {
            throw new NotFoundException(id);
        }
    }

    // method to findAll that make by Crud and Overridden
    @Override
    public Collection<Movie> findAll() {
        if (movieRepo.findAll().isEmpty()) {
            throw new NotFoundException();
        } else {
            return movieRepo.findAll();
        }
    }

    // method to add that made by Crud and Overridden
    @Override
    public Movie add(Movie movie) {
        return movieRepo.save(movie);
    }

    // method to update that made by Crud and Overridden
    @Override
    public Movie update(Movie movie) {
        if (!movieRepo.existsById(movie.getId())) {
            return null;
        }
        movieRepo.save(movie);
        return movie;

    }

    // method to deleteById that made by Crud and Overridden
    @Override
    public void deleteById(Integer id) {
        Movie mov = movieRepo.findById(id).orElseThrow(() -> new NotFoundException(id));
        mov.setCharacters(null);
        mov.setFranchise(null);
        movieRepo.deleteById(id);
    }

    // method to updateCharacters that made by Crud and Overridden
    @Override
    public void updateCharacters(int movieId, int[] characterId) {
        Movie movie = movieRepo.findById(movieId).orElseThrow(() -> new NotFoundException(movieId));
        Set<Character> characters = new HashSet<>();
        for (int id : characterId) {
            Character chara = characterRepo.findById(id).orElseThrow(() -> new NotFoundException(id));
            characters.add(chara);
        }
        movie.setCharacters(characters);
        movieRepo.save(movie);
    }

    // method to delete that made by Crud and Overridden
    @Override
    public void delete(Movie movie) {
        deleteById(movie.getId());

    }

    public Set<Character> findAllCharacters(int movId) {
        Movie movie = movieRepo.findById(movId).orElseThrow(() -> new NotFoundException(movId));
        return movie.getCharacters();
    }


}
