package com.example.moviesql.Service;

import com.example.moviesql.Models.Character;
import com.example.moviesql.Models.Franchise;
import com.example.moviesql.Models.Movie;
import com.example.moviesql.Repository.CharacterRepo;
import com.example.moviesql.Repository.FranchiseRepo;
import com.example.moviesql.Repository.MovieRepo;
import com.example.moviesql.Service.Interface.FranchiseService;
import com.example.moviesql.Utils.Exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


@Service
public class FranchiseServiceImpl implements FranchiseService {

    // Override them to add Method to each ServiceImpl
    private final FranchiseRepo franchiseRepo;

    private final MovieRepo movieRepo;

    private final CharacterRepo characterRepo;

    public FranchiseServiceImpl(FranchiseRepo franchiseRepo, MovieRepo movieRepo, CharacterRepo characterRepo) {
        this.franchiseRepo = franchiseRepo;
        this.movieRepo = movieRepo;
        this.characterRepo = characterRepo;
    }

    // method to find by id that uses by Crud
    @Override
    public Franchise findById(Integer id) {
        if (franchiseRepo.existsById(id)) {
            return franchiseRepo.findById(id).get();
        } else {
            throw new NotFoundException(id);
        }
    }

    // method to findAll that made by Crud and Overridden
    @Override
    public Collection<Franchise> findAll() {
        if (franchiseRepo.findAll().isEmpty()) {
            throw new NotFoundException();
        } else {
            return franchiseRepo.findAll();
        }
    }

    // method to add that made by Crud and Overridden
    @Override
    public Franchise add(Franchise franchise) {
        return franchiseRepo.save(franchise);
    }

    // method to update that made by Crud and Overridden
    public Franchise update(Franchise franchise) {

        if (!franchiseRepo.existsById(franchise.getId())) {
            return null;
        }
        franchiseRepo.save(franchise);
        return franchise;
    }

    // method to delete that made by Crud and Overridden
    @Override
    public void delete(Franchise franchise) {
        deleteById(franchise.getId());
    }

    // method to deleteById that made by Crud and Overridden
    @Override
    public void deleteById(Integer id) {

        if (franchiseRepo.existsById(id)) {
            Franchise fran = franchiseRepo.findById(id).get();
            fran.getMovies().forEach(s -> s.setFranchise(null));
            franchiseRepo.save(fran);
        } else {
            throw new NotFoundException(id);
        }
        franchiseRepo.deleteById(id);
    }

    // method to updateMovies that made by Crud and Overridden
    @Override
    public void updateMovies(int franchiseId, int[] movieId) {
        Franchise franchise = franchiseRepo.findById(franchiseId).orElseThrow(() -> new NotFoundException(franchiseId));
        Set<Movie> movies = new HashSet<>();
        for (int id : movieId) {
            Movie movie = movieRepo.findById(id).orElseThrow(() -> new NotFoundException(id));
            movies.add(movie);
        }
        movies.forEach(m -> {
            m.setFranchise(franchise);
        });
        franchiseRepo.save(franchise);
    }

    // method to find all movies inside franchise.
    @Override
    public Set<Movie> findAllMovies(int franchiseId) {
        Franchise franchise = franchiseRepo.findById(franchiseId).orElseThrow(() -> new NotFoundException(franchiseId));
        return franchise.getMovies();
    }

    // method to find all characters inside franchise.
    @Override
    public Set<Character> findAllCharacters(int franchiseId) {
        Set<Character> movieCharacters = new HashSet<>();
        Set<Movie> movies = findAllMovies(franchiseId);
        for (Movie m : movies) {
            movieCharacters.addAll(m.getCharacters());
        }
        return movieCharacters;
    }


}
