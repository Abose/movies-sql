package com.example.moviesql.Service;

import com.example.moviesql.Models.Character;
import com.example.moviesql.Repository.CharacterRepo;
import com.example.moviesql.Service.Interface.CharacterService;
import com.example.moviesql.Utils.Exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;


@Service
public class CharacterServiceImpl implements CharacterService {

    // Override them to add Method to each ServiceImpl
    private final CharacterRepo characterRepo;

    public CharacterServiceImpl(CharacterRepo characterRepo) {
        this.characterRepo = characterRepo;
    }


    // method to find by id that uses by Crud
    @Override
    public Character findById(Integer id) {

        if (characterRepo.existsById(id)) {
            return characterRepo.findById(id).get();
        } else {
            throw new NotFoundException(id);
        }
    }

    // method to findAll that made by Crud and Overridden
    @Override
    public Collection<Character> findAll() {
        if (characterRepo.findAll().isEmpty()) {
            throw new NotFoundException();
        } else {
            return characterRepo.findAll();
        }
    }

    // method to add that made by Crud and Overridden
    @Override
    public Character add(Character character) {
        return characterRepo.save(character);
    }

    // method to update that made by Crud and Overridden
    @Override
    public Character update(Character character) {
        if (!characterRepo.existsById(character.getId())) {
            return null;
        }
        characterRepo.save(character);
        return character;
    }

    // method to delete that made by Crud and Overridden
    @Override
    public void delete(Character character) {
        deleteById(character.getId());
    }

    // method to deleteByID that made by Crud and Overridden
    @Override
    public void deleteById(Integer id) {
        if (characterRepo.existsById(id)) {
            Character chara = characterRepo.findById(id).get();
            chara.getMovies().forEach(s -> s.setCharacters(null));
            characterRepo.save(chara);
            characterRepo.deleteById(id);
        } else {
            throw new NotFoundException(id);
        }
    }
}
