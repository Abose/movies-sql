INSERT INTO franchise(description, name)
VALUES ('Sony Pictures Entertainment is a division of Sony Corporation, a creative entertainment company built on a solid foundation of technology. Along with our sister companies, we make the movies, television, music and games that engage billions of people, connecting creators and audiences around the globe.',
        'Sony Pictures Entertainment '),
       ('A legendary producer and global distributor of filmed entertainment since 1912, Paramount Pictures library consists of more than 1,000 film titles with rights to an additional 2,500, featuring films by Hollywoods most respected filmmakers, including Martin Scorsese, J.J. Abrams and Michael Bay, among others.',
        'Paramount Pictures franchises'),
       ('The Predator franchise is a science fiction action franchise, consisting primarily of a series of films focusing on the Yautja, commonly referred to simply as the Predator, an extraterrestrial creature that hunts and kills other dangerous lifeforms, including humans, for sport and honor. Unlike the Alien franchise, which features a continuous story arc, the Predator films are more non-linear, instead focusing on individual encounters with the Predators spread across multiple timeframes. Produced by 20th Century Studios, the franchise started with the 1987 feature film Predator, continued with three sequels, Predator 2 (1990), Predators (2010) and The Predator (2018), and a prequel Prey (2022). As well as these feature films, the franchise also includes numerous expanded universe comic books, novels and video game',
        'Predator');


INSERT INTO movie(movie_title, genre, release_year, director, picture, trailer, franchise_id)
VALUES ('Spider-Man: No Way Home', 'Sci-fi', 2021, 'Jon Watts',
        'https://pbs.twimg.com/media/FbcLd2cUYAAirOG?format=jpg&name=large',
        'https://www.youtube.com/watch?v=JfVOs4VSpmA', 1),
       ('iron man 3', 'Sci-fi', 2013, 'Shane Black',
        'https://m.media-amazon.com/images/M/MV5BMjE5MzcyNjk1M15BMl5BanBnXkFtZTcwMjQ4MjcxOQ@@._V1_.jpg',
        'https://www.youtube.com/watch?v=Ke1Y3P9D0Bc', 2),
       ('Aliens vs. Predator: Requiem', 'Action', 2007, 'Greg Strause',
        'https://m.media-amazon.com/images/M/MV5BMjJiM2ZjNTEtNjQ2Zi00YjJkLWE0ZDEtYTFhODYwODk5NGMzXkEyXkFqcGdeQXVyMTExNzQzMDE0._V1_.jpg',
        'https://www.youtube.com/watch?v=oqLM_21tqyc', 3),
       ('Aliens vs. Predator', 'Action', 2004, 'Paul W. S. Anderson',
        'https://upload.wikimedia.org/wikipedia/en/f/f7/Avpmovie.jpg', 'https://www.youtube.com/watch?v=Cld40qD7HcY',
        3);


INSERT INTO character(alias, gender, full_name)
VALUES ('spiderman', 'Male', 'peter parker'),
       ('Zendaya', 'Female', 'Michelle MJ Jones-Watson'),
       ('Ironman', 'Male', 'Tony Stark'),
       ('Pepper Potts', 'Female', 'Gwyneth Paltrow'),
       ('Predator', 'Male-Alien', 'Scar'),
       ('Yautja', 'Male', 'The Yautja'),
       ('Alexa', 'Female', 'Alexa Woods');

INSERT INTO movie_characters(CHARACTER_ID, MOVIE_ID)
VALUES (1, 1),
       (2, 1),
       (3, 2),
       (4, 2),
       (5, 3),
       (6, 4),
       (7, 4);

